Kelmedix sells medical supplies to help you when you are dealing with mobility issues, breathing problems, geriatric issues, diabetes, and more. Kelmedix enhances your life with scooters, lift chairs, power wheelchairs, portable oxygen concentrators, CPAP machines, home safety equipment, and braces.

Address: 4646 Commercial Way, Spring Hill, FL 34606, USA

Phone: 352-592-1063

Website: https://kelmedix.com/

